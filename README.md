# Markdown to PDF Converter
Simple script written in PHP using `league/commonmark` converter to make html, then `spipu/html2pdf`

## Usage
1. git clone the repo, then `cd` into it's directory
2. `composer install`
3. `cd /dir/with/pdf/file`
4. `mdtopdf relative-path-to-md-file.md`

`mdtopdf` also needs to be in your PATH, or you can just use its absolute path to execute it like `/home/me/libraries/mdtopdf` or whatever

a same-named file will be written as `*.md.html` & then one as `*.pdf`

The output to `*.md.html` is slightly different than what's passed to `spipu/html2pdf`

## Customization
See the two libraries mentioned above. Edit the `mdtopdf` script to your liking. 

## Contribute
if you want
